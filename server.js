var express = require('express')
var app = express()
var fs = require('fs');
var cors = require('cors');
var originsWhitelist = [
  'http://localhost:4200'];
  var corsOptions = {
  origin: function(origin, callback){
        var isWhitelisted = originsWhitelist.indexOf(origin) !== -1;
        callback(null, isWhitelisted);
  },
  credentials:true
}
//here is the magic
app.use(cors(corsOptions));
app.use(express.static('dist'));
app.use('/favicon.ico',express.static('favicon.ico'));
app.get('/api/heroes',function(req, res) {
  // Use res.sendfile, as it streams instead of reading the file into memory.
  res.sendFile(__dirname + '/dist/heroes.json');
});
app.get('/api/heroes/:heroId',function(req, res) {
  // Use res.sendfile, as it streams instead of reading the file into memory.
  var heroes = JSON.parse(fs.readFileSync(__dirname + '/dist/heroes.json', 'utf8')).data;
  console.log(req.params.heroId);
  console.log(heroes.find(hero => hero.id == req.params.heroId));
  res.send(heroes.find(hero => hero.id == req.params.heroId));
});
//TODO: Update JSON file with hero in Put request
app.put('/api/heroes/:heroId',function(req, res) {
  // Use res.sendfile, as it streams instead of reading the file into memory.
  var heroes = JSON.parse(fs.readFileSync(__dirname + '/dist/heroes.json', 'utf8')).data;
  console.log(req.params.heroId);
  console.log(req);
  console.log(heroes.find(hero => hero.id == req.params.heroId));
  res.send(heroes.find(hero => hero.id == req.params.heroId));
});
app.use('/#',function(req, res) {
  // Use res.sendfile, as it streams instead of reading the file into memory.
  res.sendFile(__dirname + '/dist/index.html');
});

app.listen(process.env.PORT || 4200,function(){
	console.log('web server running on port 4200')
})
