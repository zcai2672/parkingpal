import { Component,OnInit } from '@angular/core';
import {ParkingZone } from '../parkingZone';
import { ParkingService } from '../services/parkingZones.service';
import { Router } from '@angular/router';


@Component({
  selector: 'map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit{
  title: string = 'My first AGM project';
  spots: ParkingZone[];
  location={};
  constructor(private parkingService: ParkingService,private router: Router) { }

  ngOnInit(): void {
    this.getParkingZones();
    if(navigator.geolocation){
          navigator.geolocation.getCurrentPosition(position => {
            this.location = position.coords;
            console.log(position.coords);
          });
       }
}

getParkingZones(): void {
/*
    this.spots=[
      {"ZoneId": 11,"name":"Zone 1", "Lat": -33.8828865,"Long":151.204, "Bays":3, "Street":"George St","Suburb":"Haymarket"},
      {"ZoneId": 12,"name":"Zone 3", "Lat": -33.8828864,"Long":151.2044274, "Bays":8, "Street":"Lee St","Suburb":"Haymarket"},
    ];
    */
    console.log(this.spots);
   this.parkingService.getParkingZones().then(parkingZones => this.spots = parkingZones);
 }
 gotoDetail(zone: ParkingZone): void {
  this.router.navigate(['/parking-payment', zone.ZoneId]);
}

}
