import { Component, OnInit } from '@angular/core';
import { PaymentService } from '../services/paymentService.service';

@Component({
  selector: 'app-parking-search',
  templateUrl: './parking-search.component.html',
  styleUrls: ['./parking-search.component.scss']
})
export class ParkingSearchComponent implements OnInit {
  payments :any[];
  constructor(private paymentService: PaymentService) { }

  ngOnInit() {
    this.payments = this.paymentService.getPayments();
  }

}
