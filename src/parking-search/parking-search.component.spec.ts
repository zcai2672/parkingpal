import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParkingSearchComponent } from './parking-search.component';

describe('ParkingSearchComponent', () => {
  let component: ParkingSearchComponent;
  let fixture: ComponentFixture<ParkingSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParkingSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParkingSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
