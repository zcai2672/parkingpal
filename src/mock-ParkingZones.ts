import { ParkingZone } from './parkingZone';
export const Zones: ParkingZone[] = [
  {"ZoneId": 11,"name":"Zone 1", "Lat": -33.8828865,"Long":151.204, "Bays":3, "Street":"George St","Suburb":"Haymarket","Rate":3.5},
  {"ZoneId": 12,"name":"Zone 3", "Lat": -33.8828864,"Long":151.2044274, "Bays":8, "Street":"Lee St","Suburb":"Haymarket", "Rate":5.5}
];
