import {ParkingZone } from './parkingZone';

export class Payment {
  Zone: ParkingZone;
  name: string;
  duration: string;
}
