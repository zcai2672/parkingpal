import { Component,Input, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { PaymentService } from '../services/paymentService.service';
import { ActivatedRoute, Params }   from '@angular/router';
import { ParkingService } from '../services/parkingZones.service';
import {ParkingZone } from '../parkingZone';
import {Payment } from '../payment';



@Component({
  selector: 'app-parking-payment',
  templateUrl: './parking-payment.component.html',
  styleUrls: ['./parking-payment.component.scss']
})
export class ParkingPaymentComponent implements OnInit {
  closeResult: string;
  parkingZone: ParkingZone;

  @Input()
  payment: Payment = <Payment>{};

    constructor(private modalService: NgbModal, private parkingService: ParkingService,  private paymentService: PaymentService ,private route: ActivatedRoute,
) {}


  ngOnInit() {
    this.route.params
  .switchMap((params: Params) => this.parkingService.getZone(+params['id']))
  .subscribe(zone => this.parkingZone = zone);
  }
  open(content:any) {
    this.payment.Zone = this.parkingZone;
    this.paymentService.addPayment(this.payment);
  this.modalService.open(content).result.then((result) => {
    this.closeResult = `Closed with: ${result}`;
  }, (reason) => {
    this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
}

private getDismissReason(reason: any): string {
  if (reason === ModalDismissReasons.ESC) {
    return 'by pressing ESC';
  } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
    return 'by clicking on a backdrop';
  } else {
    return  `with: ${reason}`;
  }
}
}
