import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { RouterModule }   from '@angular/router';
import { HttpModule }    from '@angular/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AgmCoreModule } from '@agm/core';

import { AppComponent }  from './app.component';
import { HomeComponent }   from './home.component';
import { MapComponent }   from './map/map.component';
import { ParkingPaymentComponent } from './parking-payment/parking-payment.component';
import { ParkingSearchComponent } from './parking-search/parking-search.component';
import { SidebarComponent } from './sidebar/sidebar.component';


import { ParkingService } from './services/parkingZones.service';
import { PaymentService } from './services/paymentService.service';


import { AppRoutingModule }     from './app-routing.module';

@NgModule({
  imports:      [ BrowserModule,
                  FormsModule,
                  NgbModule.forRoot(),
                  AppRoutingModule,
                  HttpModule,
                  AgmCoreModule.forRoot({
  apiKey: 'AIzaSyAEiqnysKTXG39udhMwwlF-AJNqM9izcGU'
})

  ],
  declarations: [ AppComponent, HomeComponent, MapComponent,ParkingPaymentComponent,ParkingSearchComponent, SidebarComponent],
  providers: [  ParkingService,PaymentService],
  bootstrap:    [ AppComponent ]
})


export class AppModule { }
