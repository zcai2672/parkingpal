import { Component } from '@angular/core';
@Component({
  selector: 'my-app',
  templateUrl:'./app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Tour of Heroes';
  private openSidebar : boolean = false;

  private ToggleSideBar(){
  this.openSidebar = !this.openSidebar;
}
}
