import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {

  //Property Declarations
  isActive : boolean = false;
	showMenu : string = '';
	userEmail : string = '';


  ngOnInit() {
    // Authenticate the user here
    // this.authService.getUserInfo().then(
    //   requestResult => this.userEmail =  requestResult.Data['Email']
    // )
  }


	eventCalled() {
		this.isActive = !this.isActive;
	}
	addExpandClass(element: any) {
		if (element === this.showMenu) {
			this.showMenu = '0';
		} else {
			this.showMenu = element;
		}
  }

}
