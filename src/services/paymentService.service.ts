import { Injectable } from '@angular/core';
import { Headers, Jsonp,Http,Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Payment } from '../payment';

@Injectable()
export class PaymentService {

private payments: Payment[] = [];


   constructor(private http: Http) { }

getPayments(): Payment[] {
   return this.payments;
 }
 addPayment(payment: Payment){
   this.payments.push(payment)
 }

 private handleError(error: any): Promise<any> {
   console.error('An error occurred', error); // for demo purposes only
   return Promise.reject(error.message || error);
 }

}
