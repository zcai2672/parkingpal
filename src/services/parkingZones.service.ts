import { Injectable } from '@angular/core';
import { ParkingZone } from '../parkingZone';
import { Headers, Http } from '@angular/http';
import {Zones} from '../mock-ParkingZones';


import 'rxjs/add/operator/toPromise';

@Injectable()
  export class ParkingService {
  private parkingUrl = 'http://parkingpalapinew.azurewebsites.net/api/parkingzones?lat=-27.460389&lon=153.023612&radius=0.0012';  // URL to web api
  private headers = new Headers({'Content-Type': 'application/json','Access-Control-Allow-Origin': '*'});
  private zones = Zones;
   constructor(private http: Http) { }


getZone(id: number): Promise<ParkingZone> {
  return this.getParkingZones().then(zones => zones.find(zone => zone.ZoneId === id));
}

getParkingZones(): Promise<ParkingZone[]>{
  return Promise.resolve(this.zones);
  /*
    return this.http.get(this.parkingUrl)
               .toPromise()
               .then(response => response.json().data as ParkingZone[])
               .catch(this.handleError);
               */
  }
  /*
  update(hero: Hero): Promise<Hero> {
  const url = `${this.heroesUrl}/${hero.id}`;
  return this.http
    .put(url, JSON.stringify(hero), {headers: this.headers})
    .toPromise()
    .then(() => hero)
    .catch(this.handleError);
}
*/
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
