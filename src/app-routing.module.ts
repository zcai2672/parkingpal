import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent }   from './home.component';
import { MapComponent }   from './map/map.component';
import { ParkingPaymentComponent } from './parking-payment/parking-payment.component';
import { ParkingSearchComponent } from './parking-search/parking-search.component';

const routes: Routes = [
  { path: '',  component: HomeComponent },
  { path: 'map',  component: MapComponent },
  { path: 'parking-search', component: ParkingSearchComponent },
  { path: 'parking-payment/:id', component: ParkingPaymentComponent }


];
@NgModule({
  imports: [ RouterModule.forRoot(routes,{ useHash: true }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
